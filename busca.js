//Recebendo a string de busca por parâmetro caso rode em linha de comando; Isso só deve ser usado em CLI, remover caso já tenha a String em um código
var myArgs = process.argv.slice(2);
myArgs = myArgs.join([separador = ' '])


//Carrega o arquivo/objeto JSON
let json = require('./dicas.json');

//Função de busca
function busca(stringentrada, json) {
  //Inicializa a compilação de um JSON
  var stringfinal="[";
  //"Inicializa" a string de entrada em caixa baixa e sem caracteres especiais
  stringentrada = stringentrada.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  //Navega pelos nós
  for(var i = 0; i < json.length; i++) {
    var obj = json[i];
    var obj2 = obj.dicas;
    //Se achar, adiciona à string que está sendo compilada em JSON
    if (obj.titulo.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "").includes(stringentrada)){
    var string_a_ser_inserida="{ \"posicaopai\": \""+i+"\"},";
    //Salva o tamanho da string caso precise apagar depois
    var tamanho_string_pai=string_a_ser_inserida.length;
    var stringfinal=stringfinal+string_a_ser_inserida;
  }else{
    //Reseta o tamanho pois não será apagado, já que não achou os termos de busca no título
    tamanho_string_pai=0;

  }
  var filhoanterior = false;
  //Procura pelo termo de busca dentro do nó
    for(var j = 0; j < obj2.length; j++){
    var obji = obj2[j];
    if (obji !== undefined){
      //Se achar no título, adiciona à string que está sendo compilada em JSON
    if (obji.titulo.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "").includes(stringentrada)){
      //Verifica se algum nó irmão foi adicionado antes.
      if(!filhoanterior){
        filhoanterior=true;
        //Se o título do nó pai for o resultado de busca, apagar e simplesmente apontar par o nó filho
        if(tamanho_string_pai!=0){
        stringfinal = stringfinal.slice(0, -tamanho_string_pai);
        }
        stringfinal=stringfinal+"{ \"posicaopai\": \""+i+"\" , \"posicao\": \""+j+"\"},";
  }
  //Caso positivo, só remove o "}," e acrescenta a posição do novo nó, que é irmão
    else{
        stringfinal = stringfinal.slice(0, -2);
        stringfinal=stringfinal+", \"posicao\": \""+j+"\"},";

    }
    }
    //Se achar no texto, adiciona à string que está sendo compilada em JSON
    if (obji.texto.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "").includes(stringentrada)){
      //Verifica se algum nó irmão foi adicionado antes.
      if(!filhoanterior){
        filhoanterior=true;
        //Se o título do nó pai for o resultado de busca, apagar e simplesmente apontar par o nó filho
        if(tamanho_string_pai!=0){
      stringfinal = stringfinal.slice(0, -tamanho_string_pai);
      }
      stringfinal=stringfinal+"{ \"posicaopai\": \""+i+"\" , \"posicao\": \""+j+"\"},";
      }
      //Caso positivo, só remove o "}," e acrescenta a posição do novo nó, que é irmão
      else{
        stringfinal = stringfinal.slice(0, -2);
        stringfinal=stringfinal+", \"posicao\": \""+j+"\"},";

      }

    }
    }
  }
  }
  
  //Remove a última vírgula do JSON e adiciona o último bracket
  if(stringfinal!="["){
  stringfinal = stringfinal.slice(0, -1);
  }
  stringfinal= stringfinal + "]";
  //converte as quebras de linha em \n
  stringfinal = stringfinal.replace(/\n/g, "\\n")
  //retorna o JSON
  return stringfinal; 
}


console.log(busca(myArgs,json));